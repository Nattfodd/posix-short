# posix-short

A better way of use of Posix-Spawn gem

## Installation

Add this line to your application's Gemfile:

    gem 'posix-short'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install posix-short

## Usage

require 'posix-short'

Posix.exec('echo', '"Hello!"')
