#!/bin/bash
set -e

RUBYGEMS_HOST="https://user:11111111111111111111@4af47f1d.ngrok.io"

gem_name=${PWD##*/}
echo "Building <$gem_name> gem..."
gem build "$gem_name.gemspec"
gem_file=`find . -name "$gem_name-*.gem"`
# This file is needed to trick rubygems,
# since it adds api key to auth in request
cat >~/.gem/credentials <<EOL
---
:rubygems_api_key: ''
EOL
chmod 0600 ~/.gem/credentials
gem push $gem_file --host $RUBYGEMS_HOST
